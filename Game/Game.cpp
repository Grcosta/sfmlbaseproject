#include "Precompiled.h"

Game::Game()
	:mGameWindow(sf::VideoMode(480, 500), "Pac Woman")
{
	if (!mFont.loadFromFile("../assets/font.ttf"))
		throw std::runtime_error("Unable to load the font file");

	if (!mLogo.loadFromFile("../assets/logo.png"))
		throw std::runtime_error("Unable to load the logo file");

	if (!mTexture.loadFromFile("../assets/texture.png"))
		throw std::runtime_error("Unable to load the texture file");

	mGameStates[GameState::NoCoin] = new NoCoinState(this);
	mGameStates[GameState::Playing] = new PlayingState(this);
	mGameStates[GameState::Lost] = new LostState(this, mGameStates[GameState::Playing]);
	mGameStates[GameState::Won] = new WonState(this, mGameStates[GameState::Playing]);

	changeGameState(GameState::NoCoin);
}

Game::~Game()
{
	for (GameState* gameState : mGameStates)
		delete gameState;
}

void Game::Run()
{
	sf::Clock frameClock;
	while (mGameWindow.isOpen())
	{
		sf::Event event;
		while (mGameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				mGameWindow.close();

			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::I)
					mCurrentState->insertCoin();
				if (event.key.code == sf::Keyboard::S)
					mCurrentState->pressButton();
				if (event.key.code == sf::Keyboard::Left)
					mCurrentState->moveStick(sf::Vector2i(-1, 0));
				if (event.key.code == sf::Keyboard::Right)
					mCurrentState->moveStick(sf::Vector2i(1, 0));
				if (event.key.code == sf::Keyboard::Up)
					mCurrentState->moveStick(sf::Vector2i(0, -1));
				if (event.key.code == sf::Keyboard::Down)
					mCurrentState->moveStick(sf::Vector2i(0, 1));

			}

		}
		mCurrentState->update(frameClock.restart());
		mGameWindow.clear();
		mCurrentState->draw(mGameWindow);
		mGameWindow.display();
	}

}

void Game::changeGameState(GameState::State gameState)
{
	mCurrentState = mGameStates[gameState];
}

sf::Font& Game::getFont()
{
	return mFont;
}

sf::Texture& Game::getLogo()
{
	return mLogo;
}

sf::Texture& Game::getTexture()
{
	return mTexture;
}
