#pragma once

#include "Animator.h"
#include "Character.h"

class PacWoman : public Character

{
public:
	PacWoman(sf::Texture& texture);
	void die();

	bool isDying() const;
	bool isDead() const;

	void update(sf::Time delta);

	void reset();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	bool bIsDying;
	bool bIsDead;
	sf::Sprite mVisualPacWoman;

	Animator mRunAnimation;
	Animator mDieAnimation;
};

