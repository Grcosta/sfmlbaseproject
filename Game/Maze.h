#pragma once
#include "GameState.h"
#include <vector>
#include <SFML/Graphics.hpp>

class Maze : public sf::Drawable
{
public:
	Maze(sf::Texture & texture);
	void loadLevel(std::string name);

	sf::Vector2i getPacWomanPosition() const;
	std::vector<sf::Vector2i> getGhostPositions() const;

	inline std::size_t positionToIndex(sf::Vector2i position) const;
	std::size_t positionToIndexFloat(sf::Vector2f position) const;
	inline sf::Vector2i indexToPosition(std::size_t index) const;

	sf::Vector2i mapPixelToCell(sf::Vector2f pixel) const;
	sf::Vector2f mapCellToPixel(sf::Vector2i cell) const;

	bool isWall(sf::Vector2i position) const;
	bool isWallFloat(sf::Vector2f position) const;
	bool isDot(sf::Vector2i position) const;
	bool isSuperDot(sf::Vector2i position) const;
	void pickObject(sf::Vector2i position);

	bool isBonus(sf::Vector2i position) const;

	sf::Vector2i getSize() const;

	int getRemainingDots() const;

private:

	enum CellData
	{
		Empty,
		Wall,
		Dot,
		SuperDot,
		Bonus
	};

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Vector2i mMazeSize;
	std::vector<CellData> mMazeData;
	sf::Vector2i mPacWomanPosition;
	std::vector<sf::Vector2i> mGhostPositions;

	sf::RenderTexture mRenderTextureMaze;
	sf::Texture& mTextureMaze;

};

