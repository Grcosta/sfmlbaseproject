#include "Precompiled.h"
#include <iostream>
#include <cassert>


Maze::Maze(sf::Texture & texture)
	: mMazeSize(0, 0)
	, mTextureMaze(texture)
{
}

void Maze::loadLevel(std::string name)
{
	mMazeSize = sf::Vector2i(0, 0);
	mMazeData.clear();

	mPacWomanPosition = sf::Vector2i(0, 0);
	mGhostPositions.clear();

	sf::Image levelData;
	if (!levelData.loadFromFile("../assets/levels/" + name + ".png"))
		throw std::runtime_error("Failed to load level (" + name + ")");

	mMazeSize = sf::Vector2i(levelData.getSize());

	if (mMazeSize.x < 15 || mMazeSize.y < 15)
		throw std::runtime_error("The loaded level is too small (min 15 cells large)");

	for (unsigned int y = 0; y < mMazeSize.y; y++)
	{
		for (unsigned int x = 0; x < mMazeSize.x; x++)
		{
			sf::Color cellData = levelData.getPixel(x, y);

			if (cellData == sf::Color::Black)
			{
				mMazeData.push_back(Wall);
			}
			else if (cellData == sf::Color::White)
			{
				mMazeData.push_back(Dot);
			}
			else if (cellData == sf::Color::Green)
			{
				mMazeData.push_back(SuperDot);
			}
			else if (cellData == sf::Color::Blue)
			{
				//pacwoman position
				mPacWomanPosition = sf::Vector2i(x, y);
				mMazeData.push_back(Empty);
			}
			else if (cellData == sf::Color::Red)
			{
				//ghost position
				mGhostPositions.push_back(sf::Vector2i(x, y));
				mMazeData.push_back(Empty);
			}
			else
			{
				mMazeData.push_back(Empty);
			}
		}
	}

	mRenderTextureMaze.create(32 * mMazeSize.x, 32 * mMazeSize.y);
	mRenderTextureMaze.clear(sf::Color::Black);

	sf::RectangleShape wall;
	wall.setSize(sf::Vector2f(32, 32));
	wall.setFillColor(sf::Color::Blue);

	sf::Sprite border(mTextureMaze);
	border.setTextureRect(sf::IntRect(16, 0, 16, 32));
	border.setOrigin(0, 16);

	sf::Sprite innerCorner(mTextureMaze);
	innerCorner.setTextureRect(sf::IntRect(0, 0, 16, 16));
	innerCorner.setOrigin(0, 16);

	sf::Sprite outerCorner(mTextureMaze);
	outerCorner.setTextureRect(sf::IntRect(0, 16, 16, 16));
	outerCorner.setOrigin(0, 16);

	mRenderTextureMaze.display();

	for (unsigned int i = 0; i < mMazeData.size(); i++)
	{
		sf::Vector2i position = indexToPosition(i);

		if (isWall(position))
		{
			wall.setPosition(32 * position.x, 32 * position.y);
			mRenderTextureMaze.draw(wall);

			border.setPosition(mapCellToPixel(position));
			innerCorner.setPosition(mapCellToPixel(position));
			outerCorner.setPosition(mapCellToPixel(position));

			if (!isWall(position + sf::Vector2i(1, 0)))
			{
				border.setRotation(0);
				mRenderTextureMaze.draw(border);
			}

			if (!isWall(position + sf::Vector2i(0, 1)))
			{
				border.setRotation(90);
				mRenderTextureMaze.draw(border);
			}

			if (!isWall(position + sf::Vector2i(-1, 0)))
			{
				border.setRotation(180);
				mRenderTextureMaze.draw(border);
			}

			if (!isWall(position + sf::Vector2i(0, -1)))
			{
				border.setRotation(270);
				mRenderTextureMaze.draw(border);
			}

			if (isWall(position + sf::Vector2i(1, 0)) && isWall(position + sf::Vector2i(0, -1)))
			{
				innerCorner.setRotation(0);
				mRenderTextureMaze.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(0, 1)) && isWall(position + sf::Vector2i(1, 0)))
			{
				innerCorner.setRotation(90);
				mRenderTextureMaze.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(-1, 0)) && isWall(position + sf::Vector2i(0, 1)))
			{
				innerCorner.setRotation(180);
				mRenderTextureMaze.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(0, -1)) && isWall(position + sf::Vector2i(-1, 0)))
			{
				innerCorner.setRotation(270);
				mRenderTextureMaze.draw(innerCorner);
			}

			if (!isWall(position + sf::Vector2i(1, 0)) && !isWall(position + sf::Vector2i(0, -1)))
			{
				outerCorner.setRotation(0);
				mRenderTextureMaze.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(0, 1)) && !isWall(position + sf::Vector2i(1, 0)))
			{
				outerCorner.setRotation(90);
				mRenderTextureMaze.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(-1, 0)) && !isWall(position + sf::Vector2i(0, 1)))
			{
				outerCorner.setRotation(180);
				mRenderTextureMaze.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(0, -1)) && !isWall(position + sf::Vector2i(-1, 0)))
			{
				outerCorner.setRotation(270);
				mRenderTextureMaze.draw(outerCorner);
			}
		}
	}
}

void Maze::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(sf::Sprite(mRenderTextureMaze.getTexture()), states);

	static sf::CircleShape dot = getDot();
	static sf::CircleShape superDot = getSuperDot();

	for (unsigned int i = 0; i < mMazeData.size(); i++)
	{
		sf::Vector2i position = indexToPosition(i);

		if (mMazeData[i] == Dot)
		{
			dot.setPosition(32 * position.x + 16, 32 * position.y + 16);
			target.draw(dot, states);
		}
		else if (mMazeData[i] == SuperDot)
		{
			superDot.setPosition(32 * position.x + 16, 32 * position.y + 16);
			target.draw(superDot, states);
		}
	}
}

sf::Vector2i Maze::getSize() const
{
	return mMazeSize;
}

sf::Vector2i Maze::getPacWomanPosition() const
{
	return mPacWomanPosition;
}

std::vector<sf::Vector2i> Maze::getGhostPositions() const
{
	return mGhostPositions;
}

std::size_t Maze::positionToIndex(sf::Vector2i position) const
{
	return position.y * mMazeSize.x + position.x;
}

std::size_t Maze::positionToIndexFloat(sf::Vector2f position) const
{
	return (std::floor(position.y) * std::floor(mMazeSize.x) + std::floor(position.x));
}

sf::Vector2i Maze::indexToPosition(std::size_t index) const
{
	sf::Vector2i position;

	position.x = index % mMazeSize.x;
	position.y = index / mMazeSize.x;

	return position;
}

sf::Vector2i Maze::mapPixelToCell(sf::Vector2f pixel) const
{
	sf::Vector2i cell;
	cell.x = std::floor(pixel.x / 32.f);
	cell.y = std::floor(pixel.y / 32.f);

	return cell;
}

sf::Vector2f Maze::mapCellToPixel(sf::Vector2i cell) const
{
	sf::Vector2f pixel;
	pixel.x = cell.x * 32 + 16;
	pixel.y = cell.y * 32 + 16;

	return pixel;
}

bool Maze::isWall(sf::Vector2i position) const
{
	if (position.x < 0 || position.y < 0 || position.x >= mMazeSize.x || position.y >= mMazeSize.y)
		return false;

	return mMazeData[positionToIndex(position)] == Wall;
}

bool Maze::isWallFloat(sf::Vector2f position) const
{
	if (position.x < 0 || position.y < 0 || position.x >= mMazeSize.x || position.y >= mMazeSize.y)
		return false;

	return mMazeData[positionToIndexFloat(position)] == Wall;
}

bool Maze::isDot(sf::Vector2i position) const
{
	return mMazeData[positionToIndex(position)] == Dot;
}

bool Maze::isSuperDot(sf::Vector2i position) const
{
	return mMazeData[positionToIndex(position)] == SuperDot;
}

bool Maze::isBonus(sf::Vector2i position) const
{
	return mMazeData[positionToIndex(position)] == Bonus;
}

void Maze::pickObject(sf::Vector2i position)
{
	assert(!isWall(position));
	mMazeData[positionToIndex(position)] = Empty;
}

int Maze::getRemainingDots() const
{
	int remainingDots = 0;

	for (unsigned int i = 0; i < mMazeData.size(); i++)
	{
		if (mMazeData[i] == Dot || mMazeData[i] == SuperDot)
			remainingDots++;
	}

	return remainingDots;
}
