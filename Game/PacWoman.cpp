#include "Precompiled.h"
#include <iostream>

PacWoman::PacWoman(sf::Texture& texture)
	: mVisualPacWoman(texture)
	, bIsDying(false)
	, bIsDead(false)
{
	setOrigin(20, 20);

	mRunAnimation.AddFrame(sf::IntRect(0, 32, 40, 40));
	mRunAnimation.AddFrame(sf::IntRect(0, 72, 40, 40));

	mDieAnimation.AddFrame(sf::IntRect(0, 32, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(0, 72, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(0, 112, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(40, 112, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(80, 112, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(120, 112, 40, 40));
	mDieAnimation.AddFrame(sf::IntRect(160, 112, 40, 40));

	mRunAnimation.Play(sf::seconds(0.25), true);
}

void PacWoman::die()
{
	if (!bIsDying)
	{
		mDieAnimation.Play(sf::seconds(1), false);
		bIsDying = true;
	}
}

void PacWoman::reset()
{
	bIsDying = false;
	bIsDead = false;

	mRunAnimation.Play(sf::seconds(0.25), true);
	mRunAnimation.Animate(mVisualPacWoman);
}

bool PacWoman::isDying() const
{
	return bIsDying;
}

bool PacWoman::isDead() const
{
	return bIsDead;
}

void PacWoman::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	if (!bIsDead)
		target.draw(mVisualPacWoman, states);
}

void PacWoman::update(sf::Time delta)
{
	if (!bIsDead && !bIsDying)
	{
		mRunAnimation.Update(delta);
		mRunAnimation.Animate(mVisualPacWoman);
	}
	else
	{
		mDieAnimation.Update(delta);
		mDieAnimation.Animate(mVisualPacWoman);

		if (!mDieAnimation.IsPlaying())
		{
			bIsDying = false;
			bIsDead = true;
		}
	}
	Character::Update(delta);
}
