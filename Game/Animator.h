#pragma once
#include <SFML/Graphics.hpp>

class Animator
{
public:
	Animator();

	void AddFrame(sf::IntRect frame);

	void Play(sf::Time duration, bool loop);
	bool IsPlaying() const;

	void Update(sf::Time deltaTime);
	void Animate(sf::Sprite& sprite);

private:
	std::vector<sf::IntRect> mFrames;

	bool bIsPlaying;
	sf::Time mDuration;
	bool bIsLooping;

	unsigned int mCurrentFrame;
};
