#include "Precompiled.h"
#include <iostream>

template <typename T>
void centerOrigin(T& drawable)
{
	sf::FloatRect bound = drawable.getLocalBounds();
	drawable.setOrigin(bound.width / 2, bound.height / 2);
}

GameState::GameState(Game* game)
	:mGame(game)
{
}
Game* GameState::getGame() const
{
	return mGame;
}

NoCoinState::NoCoinState(Game* game)
	: GameState(game)
{
	mNoCoinSprite.setTexture(game->getLogo());
	mNoCoinSprite.setPosition(20, 100);

	mNoCoinText.setFont(game->getFont());
	mNoCoinText.setString("Insert coin!");

	centerOrigin(mNoCoinText);
	mNoCoinText.setPosition(240, 240);

	bDisplayText = true;

}
void NoCoinState::insertCoin()
{
	getGame()->changeGameState(GameState::Playing);
}
void NoCoinState::pressButton()
{

}
void NoCoinState::moveStick(sf::Vector2i direction)
{

}
void NoCoinState::update(sf::Time delta)
{
	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	while (timeBuffer >= sf::seconds(0.5))
	{
		bDisplayText = !bDisplayText;
		timeBuffer -= sf::seconds(1);
	}
}
void NoCoinState::draw(sf::RenderWindow& window)
{
	window.draw(mNoCoinSprite);

	if (bDisplayText)
		window.draw(mNoCoinText);
}

PlayingState::PlayingState(Game* game)
	:GameState(game)
	, mMaze(game->getTexture())
	, mPacWoman(nullptr)
	, mLevel(0)
	, mLiveCount(3)
	, mScore(0)
{

	mPacWoman = new PacWoman(game->getTexture());
	mPacWoman->setMaze(&mMaze);
	mPacWoman->setPosition(mMaze.mapCellToPixel(mMaze.getPacWomanPosition()));

	resetToZero();

	ghostCount = mMaze.getGhostPositions().size();


	mCamera.setSize(sf::Vector2f(480, 480));
	mScene.create(480, 480);

	mScoreText.setFont(game->getFont());
	mScoreText.setCharacterSize(10);
	mScoreText.setPosition(10, 480);

	mLevelText.setFont(game->getFont());
	mLevelText.setCharacterSize(10);
	mLevelText.setPosition(160, 480);

	mRemainingDotsText.setFont(game->getFont());
	mRemainingDotsText.setCharacterSize(10);
	mRemainingDotsText.setPosition(280, 480);
	mRemainingDotsText.setFillColor(sf::Color::White);

	for (auto& liveSprite : mLivesSprite)
	{
		liveSprite.setTexture(game->getTexture());
		liveSprite.setTextureRect(sf::IntRect(122, 0, 20, 20));
	}

	mLivesSprite[0].setPosition(sf::Vector2f(415, 480));
	mLivesSprite[1].setPosition(sf::Vector2f(435, 480));
	mLivesSprite[2].setPosition(sf::Vector2f(455, 480));


	////TEST
	//mGhostBuffer.setFont(game->getFont());
	//mGhostBuffer.setCharacterSize(18);
	//mGhostBuffer.setPosition(280, 480);
}
void PlayingState::loadNextLevel()
{
	mMaze.loadLevel("large-level");

	mLevel++;

	int mapLevel = mLevel % 3;
	int speedLevel = std::floor(mLevel / 3);

	if (mapLevel == 0)
		mMaze.loadLevel("small");
	else if (mapLevel == 1)
		mMaze.loadLevel("medium");
	else if (mapLevel == 2)
		mMaze.loadLevel("large");

	// Destroy previous characters
	for (Ghost* ghost : mGhosts)
		delete ghost;

	mGhosts.clear();

	CreateGhosts();

	// Change speed according to the new level
	float speed = 100 + (speedLevel * 50);

	mPacWoman->setSpeed(speed + 25);

	for (auto& ghost : mGhosts)
		ghost->setSpeed(speed);

	moveCharactersToInitialPosition();

	//Update level text
	mLevelText.setString("level " + std::to_string(speedLevel) + " - " + std::to_string(mapLevel + 1));

}
void PlayingState::CreateGhosts()
{
	// Create new Ghosts
	for (auto ghostPosition : mMaze.getGhostPositions())
	{
		Ghost* ghost = new Ghost(getGame()->getTexture(), mPacWoman);
		ghost->setMaze(&mMaze);
		ghost->setPosition(mMaze.mapCellToPixel(ghostPosition));
		ghost->setSpeed(mPacWoman->getSpeed() - 25);
		mGhosts.push_back(ghost);
	}
}
void PlayingState::moveCharactersToInitialPosition()
{
	mPacWoman->setPosition(mMaze.mapCellToPixel(mMaze.getPacWomanPosition()));

	auto ghostPositions = mMaze.getGhostPositions();
	for (unsigned int i = 0; i < mGhosts.size(); i++)
		mGhosts[i]->setPosition(mMaze.mapCellToPixel(ghostPositions[i]));

	updateCameraPosition();
}
void PlayingState::updateCameraPosition()
{
	mCamera.setCenter(mPacWoman->getPosition());

	if (mCamera.getCenter().x < 240)
		mCamera.setCenter(240, mCamera.getCenter().y);
	if (mCamera.getCenter().y < 240)
		mCamera.setCenter(mCamera.getCenter().x, 240);

	if (mCamera.getCenter().x > mMaze.getSize().x * 32 - 240)
		mCamera.setCenter(mMaze.getSize().x * 32 - 240, mCamera.getCenter().y);

	if (mCamera.getCenter().y > mMaze.getSize().y * 32 - 240)
		mCamera.setCenter(mCamera.getCenter().x, mMaze.getSize().y * 32 - 240);

}
PlayingState::~PlayingState()
{
	delete mPacWoman;

	for (Ghost* ghost : mGhosts)
		delete ghost;
}
void PlayingState::insertCoin()
{
}
void PlayingState::pressButton()
{
}
void PlayingState::moveStick(sf::Vector2i direction)
{
	mPacWoman->setDirection(direction);
}
void PlayingState::resetToZero()
{
	resetLiveCount();

	mLevel = 0;
	resetCurrentLevel();

	mScore = 0;
}
void PlayingState::resetCurrentLevel()
{
	mLevel--;
	loadNextLevel();
}
void PlayingState::resetLiveCount()
{
	mLiveCount = 3;
}
void PlayingState::update(sf::Time deltaTime)
{
	mPacWoman->update(deltaTime);

	for (Ghost* ghost : mGhosts)
		ghost->update(deltaTime);

	// Timer to respawn Ghosts
	static sf::Time bufferGhost = sf::Time::Zero;


	sf::Vector2f pixelPosition = mPacWoman->getPosition();
	sf::Vector2f offset(std::fmod(pixelPosition.x, 32), std::fmod(pixelPosition.y, 32));
	offset -= sf::Vector2f(16, 16);

	if (offset.x <= 2 && offset.x >= -2 && offset.y <= 2 && offset.y >= -2)
	{
		sf::Vector2i cellPosition = mMaze.mapPixelToCell(pixelPosition);

		if (mMaze.isDot(cellPosition))
		{
			mScore += 5;
		}
		else if (mMaze.isSuperDot(cellPosition))
		{
			for (Ghost* ghost : mGhosts)
				ghost->setWeak(sf::seconds(5));

			mScore += 25;
		}
		else if (mMaze.isBonus(cellPosition))
		{
			mScore += 500;
		}

		mMaze.pickObject(cellPosition);
	}

	for (Ghost* ghost : mGhosts)
	{
		if (ghost->getCollisionBox().intersects(mPacWoman->getCollisionBox()))
		{
			if (ghost->isWeak())
			{
				mGhosts.erase(std::find(mGhosts.begin(), mGhosts.end(), ghost));
				ghostCount--;
				mScore += 100;
			}
			else
			{
				mPacWoman->die();
			}
		}
	}


	if (ghostCount == 0)
	{
		bufferGhost += deltaTime;
		if (bufferGhost.asSeconds() >= 3 && mMaze.getRemainingDots() != 0)
		{
			CreateGhosts();
			bufferGhost = sf::Time::Zero;
			ghostCount = mMaze.getGhostPositions().size();
		}
	}


	if (mPacWoman->isDead())
	{
		mPacWoman->reset();

		mLiveCount--;

		if (mLiveCount < 0)
			getGame()->changeGameState(GameState::Lost);
		else
			moveCharactersToInitialPosition();
	}


	if (mMaze.getRemainingDots() == 0)
	{
		mPacWoman->setSpeed(0);
		mPacWoman->setDirection(sf::Vector2i(0, 0));
		getGame()->changeGameState(GameState::Won);
	}

	updateCameraPosition();

	// Update score text and remaining dots
	mScoreText.setString(std::to_string(mScore) + " points");
	mRemainingDotsText.setString(std::to_string(mMaze.getRemainingDots()) + " dots");
	mGhostBuffer.setString(std::to_string(bufferGhost.asSeconds()));
}
void PlayingState::draw(sf::RenderWindow& window)
{
	mScene.clear();
	mScene.setView(mCamera);
	mScene.draw(mMaze);
	mScene.draw(*mPacWoman);

	for (Ghost* ghost : mGhosts)
		mScene.draw(*ghost);

	mScene.display();

	window.draw(sf::Sprite(mScene.getTexture()));

	window.draw(mScoreText);
	window.draw(mLevelText);
	window.draw(mRemainingDotsText);
	window.draw(mGhostBuffer);

	for (unsigned int i = 0; i < mLiveCount; i++)
		window.draw(mLivesSprite[i]);
}

WonState::WonState(Game* game, GameState* playingState)
	:GameState(game)
	, mPlayingState(static_cast<PlayingState*>(playingState))
{
	mWonText.setFont(game->getFont());
	mWonText.setString("You Won!");
	mWonText.setCharacterSize(42);
	mWonText.setFillColor(sf::Color::Green);
	centerOrigin(mWonText);
	mWonText.setPosition(240, 120);

	mNextStage.setFont(game->getFont());
	mNextStage.setString("Get ready for the next stage");
	mNextStage.setCharacterSize(16);
	mNextStage.setFillColor(sf::Color::Green);
	centerOrigin(mNextStage);
	mNextStage.setPosition(240, 300);

	bDisplayText = true;
}
void WonState::insertCoin()
{
}
void WonState::pressButton()
{
}
void WonState::moveStick(sf::Vector2i direction)
{
}
void WonState::update(sf::Time delta)
{
	static sf::Time timeBuffer = sf::Time::Zero;
	static sf::Time blinkTimer = sf::Time::Zero;
	timeBuffer += delta;
	blinkTimer += delta;

	if (timeBuffer.asSeconds() > 5)
	{
		mPlayingState->loadNextLevel();
		getGame()->changeGameState(GameState::Playing);
		timeBuffer = sf::Time::Zero;
	}

	if (blinkTimer >= sf::seconds(0.2))
	{
		bDisplayText = !bDisplayText;
		blinkTimer = sf::Time::Zero;
	}

}
void WonState::draw(sf::RenderWindow& window)
{
	window.draw(mWonText);
	if (bDisplayText)
		window.draw(mNextStage);
}

LostState::LostState(Game* game, GameState* playingState)
	:GameState(game)
	, mPlayingState(static_cast<PlayingState*>(playingState))
{
	mLostText.setFont(game->getFont());
	mLostText.setString("You lost!");
	mLostText.setCharacterSize(42);
	mLostText.setFillColor(sf::Color::Red);

	centerOrigin(mLostText);
	mLostText.setPosition(240, 120);

	mCountdownText.setFont(game->getFont());
	mCountdownText.setCharacterSize(12);

	centerOrigin(mCountdownText);
	mCountdownText.setPosition(100, 240);
}
void LostState::insertCoin()
{
	mPlayingState->resetCurrentLevel();
	mPlayingState->resetLiveCount();

	getGame()->changeGameState(GameState::Playing);
}
void LostState::pressButton()
{
}
void LostState::moveStick(sf::Vector2i direction)
{
}
void LostState::update(sf::Time delta)
{
	mContinueCount += delta;

	if (mContinueCount.asSeconds() >= 10)
	{
		mPlayingState->resetToZero();

		getGame()->changeGameState(GameState::Playing);
	}

	mCountdownText.setString("Insert a coin to continue... " + std::to_string(10 - static_cast<int>(mContinueCount.asSeconds())));

}
void LostState::draw(sf::RenderWindow& window)
{
	window.draw(mLostText);
	window.draw(mCountdownText);
}
