#pragma once

class PickUp : public sf::Drawable, public sf::Transformable
{
public:

	enum Fruit
	{
		Banana, 
		Apple, 
		Cherry,
		COUNT
	};

	PickUp(sf::Texture& texture);
	void SetFruit(Fruit fruit);
	~PickUp();

private:
	sf::Sprite mVisual;
	void draw(sf::RenderTarget& target, sf::RenderStates& states) const;
};

