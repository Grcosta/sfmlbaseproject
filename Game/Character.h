#pragma once
#include <array>
#include "Maze.h"
#include <SFML/Graphics.hpp>

class Character : public sf::Drawable, public sf::Transformable
{
public:
	Character();

	virtual void Update(sf::Time deltaTime);
	void setDirection(sf::Vector2i direction);
	sf::Vector2i getDirection() const;
	void setMaze(Maze* maze);

	void setSpeed(float speed);
	float getSpeed() const;

	bool willMove() const;

	sf::FloatRect getCollisionBox() const;

protected:
	virtual void changeDirection() {};

private:
	float mSpeed;
	Maze* mMaze;

	sf::Vector2i mCurrentDirection;
	sf::Vector2i mNextDirection;

	sf::Vector2i mPreviousIntersection;
	std::array<bool, 4> mAvailableDirections;
};

