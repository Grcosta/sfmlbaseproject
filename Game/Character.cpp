#include "Precompiled.h"

Character::Character()
	:mMaze(nullptr)
	, mSpeed(25.f)
	, mCurrentDirection(1, 0)
	, mNextDirection(0, 0)
	, mPreviousIntersection(0, 0)
{
}

sf::FloatRect Character::getCollisionBox() const
{
	sf::FloatRect bounds(3, 3, 34, 34);
	return getTransform().transformRect(bounds);
}

void Character::setMaze(Maze* maze)
{
	mMaze = maze;
}

void Character::setSpeed(float speed)
{
	mSpeed = speed;
}

float Character::getSpeed() const
{
	return mSpeed;
}

void Character::setDirection(sf::Vector2i direction)
{
	mNextDirection = direction;
}

sf::Vector2i Character::getDirection() const
{
	return mCurrentDirection;
}

bool Character::willMove() const
{
	return !mMaze->isWall(mPreviousIntersection + mNextDirection);
}

void Character::Update(sf::Time delta)
{
	sf::Vector2f pixelPosition = getPosition();

	float pixelTraveled = getSpeed() * delta.asSeconds();

	sf::Vector2f nextPixelPosition = pixelPosition + sf::Vector2f(mCurrentDirection) * pixelTraveled;
	setPosition(nextPixelPosition);

	sf::Vector2i cellPosition = mMaze->mapPixelToCell(pixelPosition);

	sf::Vector2f offset;
	offset.x = std::fmod(pixelPosition.x, 32) - 16;
	offset.y = std::fmod(pixelPosition.y, 32) - 16;

	if (mMaze->isWall(cellPosition + mCurrentDirection))
	{
		if ((mCurrentDirection.x == 1 && offset.x > 0) ||
			(mCurrentDirection.x == -1 && offset.x < 0) ||
			(mCurrentDirection.y == 1 && offset.y > 0) ||
			(mCurrentDirection.y == -1 && offset.y < 0))
		{
			setPosition(mMaze->mapCellToPixel(cellPosition));
		}
	}

	if (!mMaze->isWall(cellPosition + mNextDirection) && mCurrentDirection != mNextDirection)
	{
		if ((!mCurrentDirection.y && (offset.x > -2 && offset.x < 2)) ||
			(!mCurrentDirection.x && (offset.y > -2 && offset.y < 2)))
		{
			setPosition(mMaze->mapCellToPixel(cellPosition));
			mCurrentDirection = mNextDirection;

			if (mCurrentDirection == sf::Vector2i(1, 0))
			{
				setRotation(0);
				setScale(-1, 1);
			}
			else if (mCurrentDirection == sf::Vector2i(0, 1))
			{
				setRotation(90);
				setScale(-1, 1);
			}
			else if (mCurrentDirection == sf::Vector2i(-1, 0))
			{
				setRotation(0);
				setScale(1, 1);
			}
			else if (mCurrentDirection == sf::Vector2i(0, -1))
			{
				setRotation(90);
				setScale(1, 1);
			}
		}
	}

	static sf::Vector2i directions[4] = {
		sf::Vector2i(1, 0),
		sf::Vector2i(0, 1),
		sf::Vector2i(-1, 0),
		sf::Vector2i(0, -1)
	};

	if (cellPosition != mPreviousIntersection)
	{
		if ((!mCurrentDirection.y && (offset.x > -2 && offset.x < 2)) ||
			(!mCurrentDirection.x && (offset.y > -2 && offset.y < 2)))
		{
			std::array<bool, 4> availableDirections;

			int i = 0;
			for (auto direction : directions)
			{
				availableDirections[i] = mMaze->isWall(cellPosition + direction);
				i++;
			}

			if (mAvailableDirections != availableDirections)
			{

				mPreviousIntersection = cellPosition;
				mAvailableDirections = availableDirections;

				changeDirection();
			}
		}
	}
}
