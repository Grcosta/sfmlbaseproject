#include "Precompiled.h"
#include "PickUp.h"


PickUp::PickUp(sf::Texture & texture)
	: mVisual(texture)
{
	mVisual.setOrigin(15, 15);
	SetFruit(Banana);
}

void PickUp::SetFruit(Fruit fruit)
{
	if (fruit == Banana)
		mVisual.setTextureRect(sf::IntRect(32, 0, 30, 30));

	else if (fruit == Apple)
		mVisual.setTextureRect(sf::IntRect(32+30, 0, 30, 30));

	else if (fruit == Cherry)
		mVisual.setTextureRect(sf::IntRect(32+60, 0, 30, 30));
}

PickUp::~PickUp()
{
}

///	The draw method needs to know which fruit state it is 
/// to associate the correct sprite.

void PickUp::draw(sf::RenderTarget& target, sf::RenderStates& states) const
{
	states.transform *= getTransform();
	target.draw(mVisual, states);
}