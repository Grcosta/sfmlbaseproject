#pragma once
#include "Animator.h"
#include "PacWoman.h" 
#include "Character.h"
#include <memory>
#include <queue>

class Ghost : public Character
{
public:

	enum State
	{
		Strong,
		Weak
	};

public:
	Ghost(sf::Texture& texture, PacWoman* pacWoman);

	void setWeak(sf::Time duration);
	bool isWeak() const;

	void update(sf::Time delta);

protected:
	void changeDirection();

	sf::Vector2f CheckPositionToPacwoman(sf::Vector2f destination);

	void ChasePacWomanX();



private:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	bool bIsWeak;
	sf::Time mWeakTimeDuration;

	sf::Sprite mGhostVisual;
	PacWoman* mPacWoman;
	Maze* mMaze;
	

	Animator mStrongAnimation;
	Animator mWeakAnimation;
};
