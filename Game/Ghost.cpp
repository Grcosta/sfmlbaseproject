#include "Precompiled.h"

Ghost::Ghost(sf::Texture& texture, PacWoman* pacWoman)
	:mGhostVisual(texture)
	, bIsWeak(false)
	, mWeakTimeDuration(sf::Time::Zero)
	, mPacWoman(pacWoman)
{
	setOrigin(20, 20);

	//m_strongAnimator.addFrame(sf::IntRect(40, 32, 40, 40));
	mStrongAnimation.AddFrame(sf::IntRect(80, 32, 40, 40));

	mWeakAnimation.AddFrame(sf::IntRect(40, 72, 40, 40));
	//m_weakAnimator.addFrame(sf::IntRect(80, 72, 40, 40));

	mStrongAnimation.Play(sf::seconds(0.25), true);
	mWeakAnimation.Play(sf::seconds(1), true);

}

void Ghost::setWeak(sf::Time duration)
{
	bIsWeak = true;
	mWeakTimeDuration = duration;
}


bool Ghost::isWeak() const
{
	return bIsWeak;
}

void Ghost::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(mGhostVisual, states);
}

void Ghost::update(sf::Time delta)
{

	if (bIsWeak)
	{
		mWeakTimeDuration -= delta;

		if (mWeakTimeDuration <= sf::Time::Zero)
		{
			bIsWeak = false;
			mStrongAnimation.Play(sf::seconds(0.25), true);
		}
	}

	if (!bIsWeak)
	{
		mStrongAnimation.Update(delta);
		mStrongAnimation.Animate(mGhostVisual);
	}
	else
	{
		mWeakAnimation.Update(delta);
		mWeakAnimation.Animate(mGhostVisual);
	}

	Character::Update(delta);
}


//[SECTION] Chase PacWoman
void Ghost::changeDirection()
{
	static sf::Vector2i directions[4] = {
		sf::Vector2i(1, 0),
		sf::Vector2i(0, 1),
		sf::Vector2i(-1, 0),
		sf::Vector2i(0, -1)
	};

	std::map<float, sf::Vector2i> directionProb;

	float targetAngle;

	sf::Vector2f distance = mPacWoman->getPosition() - getPosition();

	targetAngle = std::atan2(distance.x, distance.y) * (180 / 3.14);

	for (auto direction : directions)
	{
		float directionAngle = std::atan2(direction.x, direction.y) * (180 / 3.14);

		//Normalize the angle difference
		float diff = 180 - std::abs(std::abs(directionAngle - targetAngle) - 180);

		if (!isWeak())
			directionProb[diff] = direction;
		else
			directionProb[diff] = -direction;

	}
	setDirection(directionProb.begin()->second);

	auto it = directionProb.begin();

	do
	{
		setDirection(it->second);
		it++;
	} while (!willMove());
}


sf::Vector2f Ghost::CheckPositionToPacwoman(sf::Vector2f destination)
{

	destination = mPacWoman->getPosition();
	sf::Vector2f deltaVector = getPosition() - destination;

	
	return deltaVector;
}

void Ghost::ChasePacWomanX()
{
	static sf::Vector2f directions[4] = {
	sf::Vector2f(1, 0),
	sf::Vector2f(0, 1),
	sf::Vector2f(-1, 0),
	sf::Vector2f(0, -1)};

	std::queue<sf::Vector2f> queueNodes;
	
	for (auto direction : directions)
	{
		if (!mMaze->isWallFloat(getPosition() + direction))
			sf::Vector2f newPosition = getPosition() + direction;

	}
		

}
