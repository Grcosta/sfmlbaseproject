#include "Precompiled.h"

Animator::Animator()
	: mCurrentFrame(0)
	, bIsPlaying(false)
	, mDuration(sf::Time::Zero)
	, bIsLooping(false)
{
}

void Animator::AddFrame(sf::IntRect frame)
{
	mFrames.push_back(frame);
}

void Animator::Play(sf::Time duration, bool loop)
{
	bIsPlaying = true;
	mDuration = duration;
	bIsLooping = loop;
}

bool Animator::IsPlaying() const
{
	return bIsPlaying;
}

void Animator::Update(sf::Time delta)
{
	if (!IsPlaying())
		return;

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	sf::Time frameDuration = mDuration / static_cast<float>(mFrames.size());

	while (timeBuffer > frameDuration)
	{
		mCurrentFrame++;

		if (mCurrentFrame == mFrames.size())
		{
			if (!bIsLooping)
				bIsPlaying = false;

			mCurrentFrame = 0;
		}

		timeBuffer -= frameDuration;
	}
}

void Animator::Animate(sf::Sprite& sprite)
{
	sprite.setTextureRect(mFrames[mCurrentFrame]);
}
