#pragma once
#include "Maze.h"
#include "PacWoman.h"
#include "Ghost.h"
#include <SFML/Graphics.hpp>
class Game;

class GameState
{
public:

	enum State
	{
		NoCoin,
		Playing,
		Won,
		Lost,
		Count
	};

	GameState(Game* game);
	Game* getGame() const;

	virtual void insertCoin() = 0;
	virtual void pressButton() = 0;
	virtual void moveStick(sf::Vector2i direction) = 0;
	virtual void update(sf::Time deltaTime) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;

private:
	Game* mGame;
};


class NoCoinState : public GameState
{
public:

	NoCoinState(Game* game);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void update(sf::Time Delta);
	void draw(sf::RenderWindow& window);

private:
	sf::Text mNoCoinText;
	sf::Sprite mNoCoinSprite;

	bool bDisplayText;
};

class PlayingState : public GameState
{
public:

	PlayingState(Game* game);
	~PlayingState();

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void update(sf::Time Delta);
	void draw(sf::RenderWindow& window);

	void loadNextLevel();
	void CreateGhosts();
	void resetToZero();
	void resetCurrentLevel();
	void resetLiveCount();

	void moveCharactersToInitialPosition();
	void updateCameraPosition();

private:
	PacWoman* mPacWoman;
	std::vector<Ghost*> mGhosts;
	size_t ghostCount;
	Maze mMaze;

	sf::View mCamera;
	sf::RenderTexture mScene;

	sf::Text mScoreText;
	sf::Text mLevelText;
	sf::Text mRemainingDotsText;
	sf::Sprite mLivesSprite[3];

	//TEST
	sf::Text mGhostBuffer;

	int mLevel;
	int mLiveCount;
	int mScore;
};

class LostState : public GameState
{
public:

	LostState(Game* game, GameState* playingState);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void update(sf::Time Delta);
	void draw(sf::RenderWindow& window);

private:
	sf::Text mLostText;
	sf::Time mContinueCount;
	sf::Text mCountdownText;

	PlayingState* mPlayingState;
};

class WonState : public GameState
{
public:

	WonState(Game* game, GameState* playingState);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void update(sf::Time Delta);
	void draw(sf::RenderWindow& window);

private:
	sf::Text mWonText;
	sf::Text mNextStage;
	bool bDisplayText;

	PlayingState* mPlayingState;
};

