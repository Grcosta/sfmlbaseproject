#pragma once
#include "SFML/Graphics.hpp"
#include "GameState.h"
#include <array>

class Game

{
public:
	Game();
	~Game();

	void Run();

	sf::Font& getFont();
	sf::Texture& getLogo();
	sf::Texture& getTexture();

	void changeGameState(GameState::State gameState);

private:
	sf::RenderWindow mGameWindow;
	GameState* mCurrentState;
	std::array<GameState*, GameState::Count> mGameStates;

	sf::Font mFont;
	sf::Texture mLogo;
	sf::Texture mTexture;
};

